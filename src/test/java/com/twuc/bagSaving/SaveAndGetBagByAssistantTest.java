package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SaveAndGetBagByAssistantTest {
    @Test
    void should_get_ticket_when_save_bag_in_empty_cabinet_by_assistance() {
        Cabinet cabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM), LockerSize.MEDIUM);
        assertNotNull(ticket);
    }

    @Test
    void should_get_no_ticket_when_save_bag_in_full_cabinet() {
        Cabinet cabinetWithFullLockers = CabinetFactory.createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1);
        Assistant assistant = new Assistant(cabinetWithFullLockers);
        InsufficientLockersException error = assertThrows(InsufficientLockersException.class, () -> {
            assistant.save(new Bag(BagSize.BIG), LockerSize.BIG);
        });
        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @Test
    void should_get_ticket_when_save_bag_by_one_of_assistant() {
        CabinetCloud cabinetCloud = new CabinetCloud();
        Assistant assistantWork = cabinetCloud.getAssistantWork();
        assistantWork.save(new Bag(BagSize.MEDIUM), LockerSize.MEDIUM);
    }
//
//    @Test
//    void should_get_no_ticket_when_all_cabinet_is_full() {
//        CabinetCloud cabinetCloud = new CabinetCloud();
//        CabinetFactory.createCabinetWithPlentyOfCapacity()
//    }
}
