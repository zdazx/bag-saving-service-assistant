package com.twuc.bagSaving;

public class Assistant {

    private Cabinet cabinet;

    public Assistant() {
    }

    public Assistant(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    public void setCabinet(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    public Ticket save(Bag bag, LockerSize lockerSize){
        Ticket ticket = cabinet.save(bag, lockerSize);
        return ticket;
    }

    public Bag getBag(Ticket ticket){
        return cabinet.getBag(ticket);
    }
}
