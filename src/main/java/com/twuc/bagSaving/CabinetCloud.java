package com.twuc.bagSaving;

import java.util.*;

public class CabinetCloud {

    private List<Assistant> assistants;

    private List<Cabinet> cabinets;

    public CabinetCloud() {
        LockerSetting[] settings =
                Arrays.stream(LockerSize.values()).map(size -> LockerSetting.of(
                        size,
                        Integer.MAX_VALUE)).toArray(LockerSetting[]::new);
        assistants = Arrays.asList(new Assistant(), new Assistant());
        cabinets = Arrays.asList(new Cabinet(settings), new Cabinet(settings));
    }

    public Cabinet getAnEmptyCabinetByOrder(){
        return cabinets.get(0);
    }

    public Assistant getAssistantWork(){
        Cabinet emptyCabinet = getAnEmptyCabinetByOrder();
        Assistant assistant = assistants.get(0);
        assistant.setCabinet(emptyCabinet);
        return assistant;
    }

    public void addAssistant(Assistant assistant){
        assistants.add(assistant);
    }

    public void addCabinet(Cabinet cabinet){
        cabinets.add(cabinet);
    }
}
